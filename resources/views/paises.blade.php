<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Paises</h1>
    <table>
        <thead>
            <tr>
                <th>Pais</th>
                <th>Capital</th>
            </tr>
        </thead>
    <tbody>
        @foreach ($paises as $pais => $infopais)
            <td> {{ $pais }} </td>
            <td> {{$infopais["capital"]}} </td>
        @endforeach
    </tbody>
    </table>
</body>
</html>