<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

//Primeras ruta

Route::get('hola', function () {
    echo "Hola estoy en laravel";
});

Route::get('arreglo', function () {
    //Arreglo estudiantes

    $estudiantes = ["AD" => "andres", "CD" => "cata", "BR" => "brayan", "s" => "sadas"];
    //var_dump($estudiantes);

    foreach ($estudiantes as $indice => $estudiante) {
        echo "$estudiante tiene indice $indice<hr>";
    }

});

Route::get('paises', function () {

    $paises = [
        "Colombia" => [
            "capital" => "Bogota",
            "moneda" => "peso",
            "poblacion" => 50.372,
        ],
        "Ecuador" => [
            "capital" => "Quito",
            "moneda" => "dolar",
            "poblacion" => 17.57,
        ],
        "Brazil" => [
            "capital" => "Brasilia",
            "moneda" => "real",
            "poblacion" => 212.216,
        ],
        "Bolivia" => [
            "capital" => "La Paz",
            "moneda" => "boliviano",
            "poblacion" => 11.633,
        ],
    ];

    //Recorrer la primera dimension del arreglo
    foreach ($paises as $pais => $infopais) {
        echo $pais ."<br>";
    }

    foreach ($paises as $pais => $infopais) {
        echo "<h2> $pais <br>";
        echo "capital: ".$infopais['capital']."<br>";
        echo "moneda: ".$infopais["capital"] ."<br>";
        echo "poblacion(en millones de habitantes):".$infopais["capital"]."<br><hr/>";

    }
    
    return view("paises")->with("paises", $paises);

});
